import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

public class Client {

	public static void main(String[] args) {
 
        String hostname = "localhost"; //args[0];
        int port = 17;//Integer.parseInt(args[1]);
 
        try {
            InetAddress address = InetAddress.getByName(hostname);
            DatagramSocket socket = new DatagramSocket();
 
            String ruta = "./enviados/copia.pptx";
            FileOutputStream fos = new FileOutputStream(ruta);
            
    		int read = 0;
    		int remaining = Integer.MAX_VALUE;

    		DatagramPacket request = new DatagramPacket(new byte[1], 1, address, port);
    		socket.send(request);
            
            while (true) {
 
                
                
                
 
                byte[] buffer = new byte[4096];
                DatagramPacket response = new DatagramPacket(buffer, buffer.length);
                
                socket.receive(response);
 
                read = response.getLength();
                String quote = new String(buffer, 0, read);
                
                
                System.out.println(quote);
                if(quote.startsWith("SIZE:")) {
                	remaining = Integer.parseInt(quote.split(":")[1]);
                }
                //Escritura del archivo
                else {
                	System.out.println(quote);
                	remaining -= response.getLength();
                	fos.write(buffer, 0, read);
                }
        		
                
 
 
                Thread.sleep(1000);
            }
 
        } catch (SocketTimeoutException ex) {
            System.out.println("Timeout error: " + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Client error: " + ex.getMessage());
            ex.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
	
	private void receiveFile() {
		
	}
	
}
